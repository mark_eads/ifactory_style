$(function ($) {

    // mobile side-menu slide toggler
    var $menu = $("#sidebar-nav");

    // navbar notification popups
    $(".notification-dropdown").each(function (ix, el) {
        var $el = $(el);
        var $dialog = $el.find(".pop-dialog");
        var $trigger = $el.find(".trigger");

        $dialog.click(function (e) {
            e.stopPropagation();
        });

        $dialog.find(".close-icon").click(function (e) {
            e.preventDefault();
            $dialog.removeClass("is-visible");
            $trigger.removeClass("active");
        });

        $("body").click(function () {
            $dialog.removeClass("is-visible");
            $trigger.removeClass("active");
        });

        $trigger.click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            // hide all other pop-dialogs
            $(".notification-dropdown .pop-dialog").removeClass("is-visible");
            $(".notification-dropdown .trigger").removeClass("active");

            $dialog.toggleClass("is-visible");
            if ($dialog.hasClass("is-visible")) {
                $(this).addClass("active");
            } else {
                $(this).removeClass("active");
            }
        });
    });


    // sidebar menu dropdown toggle
    $("#dashboard-menu a.dropdown-toggle").each(function (ix, el) {
        var $el = $(el),
            $item = $el.parent();

        $el.on('click', function (e) {
            e.preventDefault();

            $item.toggleClass("active");

            if ($item.hasClass("active")) {
                $item.find(".submenu").slideDown("fast");
            } else {
                $item.find(".submenu").slideUp("fast");
            }
        });
    });


    $("body").click(function () {
        if ($(this).hasClass("menu")) {
            $(this).removeClass("menu");
        }
    });

    $menu.click(function (e) {
        e.stopPropagation();
    });

    $("#menu-toggler").click(function (e) {
        e.stopPropagation();
        $("body").toggleClass("menu");
    });

    (function toggleDevFeatures(forceShow) {
        var loc = window.location.host;
        if (loc.indexOf('localhost') > -1 || forceShow) {
            $('.localhost').removeClass('localhost');
        }
    }());

    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
        //        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });

    $(document).ready(function () {
        var $main = $('#main-content'),
            $sidebar = $('#sidebar'),
            $menu = $sidebar.find('.sidebar-menu'),
            $header = $('header'),
            $container = $("#container"),
            inResize = false;

        function checkWidth() {
            if (inResize) { return; }

            if ($(window).width() < 1025) {
                hideNav();
            } else {
                showNav();
            }

            retriggerResize();
        }

        function toggleNav() {
            if ($menu.is(":visible")) {
                hideNav();
            } else {
                showNav();
            }

            retriggerResize();
        }

        function retriggerResize() {
            var intCb,
                timCb,
                clrCb;

            inResize = true;

            window.clearInterval(intCb);

            intCb = window.setInterval(function () {
                $(window)
                    .trigger('resize');

                window.clearTimeout(timCb);

                timCb = window.setTimeout(function () {
                    $(window)
                        .trigger('resize');

                    window.clearTimeout(clrCb);

                    clrCb = window.setTimeout(function () {
                        inResize = false;
                        window.clearInterval(intCb);
                    }, 75);

                }, 300);

            }, 50);
        }

        function hideNav() {
            $main.css({ 'padding-left': '0' });

            $sidebar.css({ 'margin-left': $sidebar.outerWidth() * -1 });

            $menu.hide();
            $container.addClass("sidebar-closed");
        }

        function showNav() {
            $main.css({ 'padding-left': $sidebar.outerWidth() });

            $menu.show();
            $sidebar.css({ 'margin-left': '0' });

            $container.removeClass("sidebar-closed");
        }

        $('.navcollapser')
            .on('click', toggleNav);

        $(window)
            .on('resize', checkWidth)
            .trigger('resize');

        $('#main-content, #sidebar').css({
            'padding-top': $header.outerHeight()
        });
    });
}(jQuery));
