﻿

function dispatchPanel(input) {

    //input = input || {
    //    PanelId: 107,
    //    PersistResult: false,
    //    GetNextRouteStepInputModels: [
    //        { WipId: 70513, OperationStatus: 'PASS' },
    //        { WipId: 70514, OperationStatus: 'PASS' },
    //        { WipId: 70515, OperationStatus: 'PASS' }
    //    ]
    //};


    var onSuccess = function (data) {

        for (var i = 0; i < data.GetNextRouteStepResults.length; i++) {
            console.log("SN: " + data.GetNextRouteStepResults[i].SerialNumber);
            console.log("RuleName: " + data.GetNextRouteStepResults[i].RuleName);

            for (var j = 0; j < data.GetNextRouteStepResults[i].InQueueRouteSteps.length; j++) {
                console.log("Destination: " + data.GetNextRouteStepResults[i].InQueueRouteSteps[j].RouteStepName);
            }
        }

        console.log(data);

        $.colorbox.close();
    };

    var onFail = function (data) {
        var message = "";
        if (data.length > 0) {
            for (var i in data) {
                message += (data[i].ExceptionMessage || data[i].ErrorMessage || data[i].Message);
            }
        } else {
            message = data.ExceptionMessage || data.ErrorMessage || data.Message;
        }
        if (message)
            console.log(message);
        else {
            console.log("Routing failed, but no failure message could be retrieved.");
        }
        $.colorbox.close();
    };

    angular.element(document.getElementById('test_scripts'))
        .scope()
        .actions
        .runTestScript('/api/routing/dispatchPanel', input, onSuccess, onFail);
}

function completePanel(input) {

    //input = input || {
    //    PanelId: 98,
    //    CompletePanelWipModels: [
    //        { WipId: 70472, OperationStatus: 'PASS' },
    //        { WipId: 70473, OperationStatus: 'PASS' },
    //        { WipId: 70474, OperationStatus: 'FAIL' }
    //    ]
    //};

    var onSuccess = function (data) {
        console.log('Panel Completed Successfully');
        $.colorbox.close();
    };

    var onFail = function (data) {
        var message = "";
        if (data.length > 0) {
            for (var i in data) {
                message += (data[i].ExceptionMessage || data[i].ErrorMessage || data[i].Message);
            }
        } else {
            message = data.ExceptionMessage || data.ErrorMessage || data.Message;
        }
        if (message)
            console.log(message);
        else {
            console.log("Validation failed, but no failure message could be retrieved.");
        }
        $.colorbox.close();
    };

    angular.element(document.getElementById('test_scripts'))
        .scope()
        .actions
        .runTestScript('/api/wip/completePanel', input, onSuccess, onFail);
}



function testScript_US6913(input) {
    var onSuccess = function(data) {
        console.log("Panel passed validations.");
        $.colorbox.close();
    };

    var onFail = function(data) {
        var message = "";
        if (data.length > 0) {
            for (var i in data) {
                message += (data[i].ExceptionMessage || data[i].ErrorMessage || data[i].Message);
            }
        } else {
            message = data.ExceptionMessage || data.ErrorMessage || data.Message;
        }
        
        if (message)
            console.log(message);
        else {
            console.log("Validation failed, but no failure message could be retrieved.");
        }
        $.colorbox.close();
    };

    angular.element(document.getElementById('add_failure'))
        .scope()
        .actions
        .testPanelStartValidations(input, onSuccess, onFail);
}

function prestartPanel(input) {
    //{ PanelId: xx }
    var onSuccess = function (data) {
        if (data.FailureMessage.length > 0) {
            console.log("PreStart Rule: " + data.PanelPreStartName);
            console.log("Failure Message: " + data.FailureMessage);
        } else {
            console.log("Panel passed Prestart checks");
        }
        $.colorbox.close();
    }

    var onFail = function (data) {
        var message = "";
        if (data.length > 0) {
            for (var i in data) {
                message += (data[i].ExceptionMessage || data[i].ErrorMessage || data[i].Message);
            }
        } else {
            message = data.ExceptionMessage || data.ErrorMessage || data.Message;
        }
        if (message)
            console.log(message);
        else {
            console.log("There was an error with running PreStart checks, but no failure message could be retrieved.");
        }
        $.colorbox.close();
    };

    angular.element(document.getElementById('test_scripts'))
        .scope()
        .actions
        .runTestScript('/api/routing/prestartPanel', input, onSuccess, onFail);
}